
# Camera.Device
vendor/lib/camera.device@1.0-impl.so
vendor/lib/camera.device@3.2-impl.so
vendor/lib/camera.device@3.3-impl.so
vendor/lib/camera.device@3.4-impl.so
vendor/lib/camera.device@3.5-impl.so
vendor/lib64/camera.device@1.0-impl.so
vendor/lib64/camera.device@3.2-impl.so
vendor/lib64/camera.device@3.3-impl.so
vendor/lib64/camera.device@3.4-impl.so
vendor/lib64/camera.device@3.5-impl.so

# Camera.Device (External)
vendor/lib/camera.device@3.4-external-impl.so
vendor/lib/camera.device@3.5-external-impl.so
vendor/lib/camera.device@3.6-external-impl.so
vendor/lib64/camera.device@3.4-external-impl.so
vendor/lib64/camera.device@3.5-external-impl.so
vendor/lib64/camera.device@3.6-external-impl.so

# Camera.Device (Hardware)
vendor/lib/vendor.qti.hardware.camera.device@1.0.so
vendor/lib/vendor.qti.hardware.camera.device@2.0.so
vendor/lib/vendor.qti.hardware.camera.device@3.5.so
vendor/lib64/vendor.qti.hardware.camera.device@1.0.so
vendor/lib64/vendor.qti.hardware.camera.device@2.0.so
vendor/lib64/vendor.qti.hardware.camera.device@3.5.so

# Camera Provider
vendor/bin/hw/android.hardware.camera.provider@2.4-service_64
vendor/etc/init/android.hardware.camera.provider@2.4-service_64.rc
vendor/lib/android.hardware.camera.provider@2.4-legacy.so
vendor/lib/hw/android.hardware.camera.provider@2.4-impl.so
vendor/lib64/android.hardware.camera.provider@2.4-external.so
vendor/lib64/android.hardware.camera.provider@2.4-legacy.so
vendor/lib64/hw/android.hardware.camera.provider@2.4-impl.so

# MIUI Camera
-system/lib/libcamera_algoup_jni.xiaomi.so
-system/lib/libcamera_mianode_jni.xiaomi.so
-system/lib64/libcamera_algoup_jni.xiaomi.so
-system/lib64/libcamera_mianode_jni.xiaomi.so
vendor/camera/fonts/FZMiaoWuJW.ttf
vendor/camera/mimoji/data.zip
vendor/camera/mimoji/model2.zip
vendor/camera/model/model_front.dlc

# MIUI Camera
-product/priv-app/MiuiCamera/MiuiCamera.apk;OVERRIDES=Aperture,Camera2,GoogleCameraGo,Snap|0fb0b6145fbe8f6c48f43554cd62cf3e2b4dd4de

# MIUI Scanner
-product/data-app/MiuiScanner/MiuiScanner.apk:product/priv-app/MiuiScanner/MiuiScanner.apk|859367255cdaf84e22d199176638d4d07bf63b16
