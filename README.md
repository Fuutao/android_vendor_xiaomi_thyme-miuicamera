# android_vendor_xiaomi_thyme-miuicamera

Prebuilt stock MIUI Camera for Xiaomi 10S (thyme), to include in custom ROM builds.

Extracted from thyme MIUI package (refer proprietary-files.txt for version).

### How to use?

1. Clone this repo to `vendor/xiaomi/thyme-miuicamera`

2. Inherit it from `device.mk` in device tree:

```
# MIUI amera
$(call inherit-product, vendor/xiaomi/thyme-miuicamera/thyme-miuicamera-vendor.mk)
```
